# 浙江大华摄像头Web3.0网页播放SDK插件包

## 简介
本仓库提供了一个用于浙江大华摄像头的Web3.0网页播放SDK插件包。通过设置好DDNS和端口映射，您可以使用本SDK包在网页上播放远程摄像头的实时视频和历史视频。

## 功能特点
- **实时视频播放**：支持在网页上实时播放远程摄像头的视频流。
- **历史视频回放**：支持播放摄像头的历史视频记录。
- **简单易用**：只需设置好DDNS和端口映射，即可快速集成到您的网页应用中。

## 使用方法
1. **设置DDNS和端口映射**：确保您的摄像头已正确配置DDNS和端口映射，以便能够通过互联网访问。
2. **下载SDK插件包**：从本仓库下载最新的SDK插件包。
3. **集成到网页**：将SDK插件包集成到您的网页应用中，按照提供的文档进行配置和初始化。
4. **播放视频**：在网页上播放摄像头的实时视频或历史视频。

## 注意事项
- 请确保您的网络环境支持DDNS和端口映射，否则可能无法正常播放视频。
- 在使用过程中，请遵守相关法律法规，确保视频内容的合法性。

## 支持与反馈
如果您在使用过程中遇到任何问题或有任何建议，欢迎通过仓库的Issue功能提交反馈。我们将尽快为您提供帮助。

---

希望本SDK插件包能够帮助您轻松实现网页上的摄像头视频播放功能！